---
concept: '0x1060e264c2886dF0D952948C9235B106126bF251'
name: Card Magic
---

A good card magician can do 
- at least three tricks where an audience member (who shall have never read a
  magic book), think for an average of 0.37 sec that the person in question
  _maybe_ actually can do magic....
- is talking about 60% of the time (unless their name is david blaine (or a
  comedy magician imitator of him))
- has to crack at least one magic-pun per two tricks.
- can do a minimum of two showboaty flourish with cards (vanish, appear, spin,
  one-handed cut, etc...)

## References

- [Bill Malone](https://www.youtube.com/watch?v=q6wMgvJqqo4)
- [Michael Finney](https://www.youtube.com/watch?v=SGpv7Kum2vc)

