const fathom = require('fathom-contracts')
const ethers = require('ethers')
const ipfsAPI = require('ipfs-api')
const ipfs = ipfsAPI('ipfs.infura.io', '5001', {protocol: 'https'})

//UI
const ora = require('ora')
const chalk = require('chalk')
let spinner = ora(chalk.yellow("Creating a new concept"))

// Network
let DEPLOYER_ADDRESS = '0xa31D49D09F8C89967cA4F7Ec54BEAf1fCe7C84Fb'

let network = 'kovan'
let networkID = 42
let address = fathom.ConceptRegistry.networks[networkID].address
let abi = fathom.ConceptRegistry.abi

// Account
let provider = ethers.getDefaultProvider(network)
let mnemonic = process.argv[2]
if (!mnemonic) {
  try {
    mnemonic = require('../mnemonic.json')
  }
  catch (e) {
    throw new Error(chalk.red('no mnemonic passed or mnemonic.json file found'))
  }
}
let wallet = (new ethers.Wallet.fromMnemonic(mnemonic)).connect(provider)

async function main () {

  let ConceptRegistry = new ethers.Contract(address, abi, wallet)
  let mew = await ConceptRegistry.mewAddress()

  spinner.start()
  let tx = await ConceptRegistry.makeConcept([mew], [1000], 60 * 60 * 24, '0xaa', DEPLOYER_ADDRESS, '0x0000000000000000000000000000000000000000')
  spinner.text = chalk.yellow("Mining transaction")
  let results = await tx.wait()

  let conceptAddress = results.events[0].args._concept
  spinner.stop()

  console.log(chalk.green('concept address: ') + chalk.bold.green(conceptAddress))
}

main()
