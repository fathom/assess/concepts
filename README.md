# Fathom Concepts

The fathom network rests on an ontology of concepts which define the communities
people get assessed and join. They're defined by resources 

This repository is a community driven tool to maintain concepts and their definitions.

Each concept is represented by a markdown document in the [concepts](./concepts)
folder. Anyone can propose changes to these, which'll get merged in if accepted.
Currently the maintainers (i.e the fathom dev team) are the only ones who have
merge rights. 

Once changes are merged into the master branch, we run a Gitlab Pipeline which
updates the definitions on-chain (currently only on the kovan-testnet).

In order to do this the concept that's being updated needs to be owned by the
account, `0xa31D49D09F8C89967cA4F7Ec54BEAf1fCe7C84Fb`.
